import { CardType, UseBerryItemType } from "../Types";

type CardProps = {
  cardIndex: number;
  cardContent: CardType;
  handleGenerateCard: (guid: string, _cardIndex: number) => void;
};

const Card = ({ cardIndex, cardContent, handleGenerateCard }: CardProps) => {
  return (
    <li key={cardIndex}>
      <div className="Card-class">
        <ol className="Useberry-item-container">
          {cardContent.map((item: UseBerryItemType, _index: number) => (
            <li
              className="Useberry-item"
              key={item.guid}
              style={{
                backgroundColor: `${item.color}`,
              }}
              onClick={() => handleGenerateCard(item.guid, cardIndex)}
            >
              {item.title}
            </li>
          ))}
        </ol>
      </div>
    </li>
  );
};

export default Card;
