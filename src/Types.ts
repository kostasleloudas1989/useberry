export type UseBerryItemType = {
  title: string;
  guid: string;
  options: UseBerryItemType[];
  color: string;
};

export type UseBerryItemNodeType = {
  title: string;
  guid: string;
  selectionChild: UseBerryItemNodeType | undefined;
  color: string;
};

export type CardType = UseBerryItemType[];
