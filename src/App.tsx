import axios from "axios";
import { useEffect, useState } from "react";
import "./App.css";
import Card from "./components/Card";
import { CardType, UseBerryItemNodeType } from "./Types";

const apiUri = process.env.REACT_APP_API_URI;

function App() {
  // State
  const [data, setData] = useState<CardType>();
  const [cards, setCards] = useState<CardType[]>([]);
  const [cardIndexesShown, setCardIndexesShown] = useState<number[]>([]);
  const [history, setHistory] = useState<UseBerryItemNodeType>();

  // LifeCycle
  useEffect(() => {
    return () => {
      FetchDataQuery();
    };
  }, []);

  useEffect(() => {
    const initialCards: CardType = [];
    if (data) {
      for (const item of data) {
        initialCards.push(item);
      }
      setCards([initialCards]);
    }
  }, [data]);

  // Handlers
  const FetchDataQuery = async () => {
    try {
      const response = await axios.get(`${apiUri}`);
      const _data = response.data;
      if (_data) setData(_data);
    } catch (error) {
      console.error(error);
    }
  };

  const renderCards = () => {
    return (
      <ol
        className="CardContainer"
        style={{ display: "flex", flexDirection: "row", listStyle: "none" }}
      >
        {cards.map((card: CardType, _cardIndex: number) => (
          <Card
            key={_cardIndex}
            cardIndex={_cardIndex}
            cardContent={card}
            handleGenerateCard={handleGenerateCard}
          />
        ))}
      </ol>
    );
  };

  const handleGenerateCard = (guid: string, _cardIndex: number) => {
    const foundCard = cards[_cardIndex];
    const foundItem = foundCard.find((item) => item.guid === guid);
    const doubleEntryFound = cards.find((x) => x === foundItem?.options);
    if (cardIndexesShown?.includes(_cardIndex) === false) {
      setCardIndexesShown([...cardIndexesShown, _cardIndex]);
    }
    if (
      foundItem &&
      foundItem.options?.length > 0 &&
      doubleEntryFound == undefined &&
      cardIndexesShown?.includes(_cardIndex) === false
    ) {
      setCards([...cards, foundItem.options]);

      let newItem: UseBerryItemNodeType = {
        title: foundItem.title,
        guid: foundItem.guid,
        color: foundItem.color,
        selectionChild: undefined,
      };

      if (history) {
        nestToChild(history, newItem);
      } else {
        setHistory(newItem);
      }
    }
  };

  const nestToChild = (
    item: UseBerryItemNodeType,
    child: UseBerryItemNodeType
  ) => {
    if (item.selectionChild) {
      nestToChild(item.selectionChild, child);
    } else {
      item.selectionChild = child;
    }
    setHistory(item);
  };

  return (
    <div
      className="App"
      style={{
        display: "flex",
        flexDirection: "column",
      }}
    >
      {renderCards()}
      <input
        className="Save-button"
        type="button"
        value="save"
        onClick={() => console.log(history)}
      />
    </div>
  );
}

export default App;
