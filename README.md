#

This project was created with create-react-app with Typescript template.

## This Project is my Response to the following excerice:

The API provided returns an array. Every element of this array is an object that we will call useberryItem.

Every useberryItem object has the following properties:

title: String
guid: unique object id
options : Array of useberryItem objects
color: Hex value of a color
\*The API always returns random results.

Implement a code in React that:

1. At first, will have a div with the useberryItems of the initial array and as a background color of each element, asign the color property of each object.

2. When the user clicks on to any element of the array, a new div will be shown (with a small margin), populated by the elements that are inside the selected element's options property, each one of these elements to have as background color its respected object's color property. This process continues until there are no more nested items to show.

3. Finally, implement a global button save that will log all of the selected elements of the user into a new nested object that follows the structure:

title: the title of the first item that the user has chosen
guid : the guid of the first item that the user has chosen
color: the color of the first item that the user has chosen
selectionChild: the object of the next item that the user has chosen etc . . .

## Instructions

- Paste the provided .env file at the project root directory.

- Run npm install || yarn install, to install project dependencies.

- Run npm start to execute.
